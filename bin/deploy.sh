#!/bin/bash

containsElement() {
  local e
  for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
  return 1
}

host="serwer1370363@serwer1370363.home.pl"
destination="/home/serwer1370363/public_html/ojczystaczystacmswww"
rsync_chmod=--chmod=D0755,F0644
rsync_ssh_key="~/.ssh/mosqito_deploy"
stages=("pl" "en")

if [ -f "bin/exclude.txt" ]; then
    exclude="--exclude-from=bin/exclude.txt"
else
    if [ -f "exclude.txt" ]; then
        exclude="--exclude-from=exclude.txt"
    fi
fi

while [[ $# > 0 ]]
do
key="$1"

case ${key} in
    -s|--stage)
    if containsElement "$2" "${stages[@]}"; then
        stage="${2}"
    else
        stage="pl"
    fi

    shift
    ;;
    -g|--gulp)
    gulp=yes
    ;;
    -r|--dry-run)
    dry=yes
    ;;
    -h|--help)
    echo "Available options:
    -r | --dry-run  use dry run instead of real upload
    -g | --gulp     Use gulp
    -s | --stage    Chose lang version (pl|en) default pl"
    exit
    ;;
    *)
    echo "undefiend commands"
    exit
esac
shift
done

if [ "$dry" == yes ]; then
    echo "Dry Run"
fi

if [ "$gulp" == yes ]; then
    echo "Run gulp task"
    rm -rf public/css public/js
    gulp --production
fi

echo "Copy production shared/${stage}/.env file"
cp shared/${stage}/.env ./
cp shared/${stage}/.htaccess ./public/

if [ "$dry" == yes ]; then
    rsync --dry-run ${exclude} . "$host:$destination"
else
    rsync -avz --progress ${exclude} ${rsync_chmod} . "$host:$destination"
fi
echo "Restore local .env file"
cp shared/local/.env ./
cp shared/local/.htaccess ./public/

echo "Run post_deploy.sh script"
ssh ${host} "sh $destination/bin/post_deploy.sh"

if [ "$gulp" == yes ]; then
    echo "Run gulp task"
    rm -rf public/css public/js
    gulp
fi

