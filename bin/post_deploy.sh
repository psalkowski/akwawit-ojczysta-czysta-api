#!/bin/sh

alias php=php56-cli
dir=/home/serwer1370363/public_html/ojczystaczystacmswww
# find $dir -type d -print0 | xargs -0 chmod 0755 # for directories
# find $dir -type f -print0 | xargs -0 chmod 0644 # for files

echo "test" > $dir/test.txt

find $dir -type d -exec chmod 755 {} +
find $dir -type f -exec chmod 644 {} +

chmod -R 777 $dir/storage

php $dir/composer.phar dump-autoload
php $dir/artisan migrate --force
php $dir/artisan clear-compiled
php $dir/artisan cache:clear
php $dir/artisan config:clear
php $dir/artisan route:clear
php $dir/artisan view:clear