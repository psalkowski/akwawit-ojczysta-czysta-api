process.env.DISABLE_NOTIFIER = true

var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.copy('resources/assets/bower/bootstrap-sass/assets/fonts', 'public/fonts');
    mix.copy('resources/assets/bower/tinymce/skins/lightgray/fonts', 'resources/assets/fonts');
    mix.copy('resources/assets/bower/tinymce/skins/lightgray/fonts', 'public/fonts');
    mix.copy('resources/assets/bower/tinymce/plugins', 'public/js/plugins');
    mix.copy('resources/assets/bower/tinymce/skins', 'public/js/skins');
    mix.copy('resources/assets/images', 'public/images');

    mix.styles([
    ], 'public/css/vendor.css');

    mix.scripts([
        '../bower/jquery/dist/jquery.js',
        '../bower/jquery-ui/jquery-ui.js',
        '../bower/bootstrap-sass/assets/javascripts/bootstrap.js',
        '../bower/tinymce/tinymce.js',
        '../bower/tinymce/themes/modern/theme.js'
    ], 'public/js/vendor.js');

    mix.scriptsIn('resources/assets/js');
    mix.sass('app.scss');
});
