<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'to@mosqi.to',
            'password' => bcrypt('3HPHYgSp5W12UiLSFdol')
        ]);
    }
}
