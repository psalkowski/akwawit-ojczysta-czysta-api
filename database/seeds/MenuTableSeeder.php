<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->delete();
        DB::table('menus')->insert([[
            'name' => 'Śniadania',
            'template' => 'sniadania.html',
            'price' => '12',
            'position' => 0
        ], [
            'name' => 'Obiady',
            'template' => 'obiady.html',
            'price' => '17',
            'position' => 1
        ], [
            'name' => 'Zakąski do wódki',
            'template' => 'zakaski.html',
            'price' => '9',
            'position' => 2
        ], [
            'name' => 'Zestawy z wódką',
            'template' => 'zestawy.html',
            'price' => '40',
            'position' => 3
        ], [
            'name' => 'Alkohole',
            'template' => 'alkohole.html',
            'price' => null,
            'position' => 4
        ], [
            'name' => 'Napoje',
            'template' => 'napoje.html',
            'price' => null,
            'position' => 5
        ]]);
    }
}
