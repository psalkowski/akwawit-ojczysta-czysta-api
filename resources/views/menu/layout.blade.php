@extends('layout')

@section('body')

    <ul class="toaster">
        @if(Session::has('status'))
            <li class="alert alert-{{ Session::get('status') }} alert-dismissible" role="alert">
                {{ Session::get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </li>
        @endif
    </ul>

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default" id="panel-form">
                <div class="panel-heading">
                    <h3 class="panel-title">Formularz</h3>
                </div>
                <div class="panel-body">
                    @yield('form')

                    <div class="form-disabled"></div>
                </div>
            </div>

            @yield('list')
        </div>

        <div class="col-md-4">
            <div class="panel panel-default panel-sortable" data-url="{{ route('menu.position') }}">
                <div class="panel-heading">
                    <h3 class="panel-title">Rodzaje menu</h3>

                    <div class="panel-action">
                        <button class="btn btn-default btn-cancel"><span class="glyphicon glyphicon-remove"></span></button>
                        <button class="btn btn-default btn-ok" data-url="{{ route('slider.position') }}"><span class="glyphicon glyphicon-ok"></span></button>
                        <button class="btn btn-default btn-sort"><span class="glyphicon glyphicon-sort"></span></button>
                    </div>
                </div>

                <div class="list-group">
                    @forelse($entities as $idx => $entity)
                        <a href="{{ route('menu.edit', $entity->id) }}" data-id="{{ $entity->id }}" class="list-group-item {{ Request::url() == (route('menu.edit', $entity->id)) ? 'active' : null }}">
                            <span class="list-group-element">
                                <span class="number">{{$idx + 1}}.</span> {{ $entity->name }}
                            </span>
                        </a>
                    @empty
                        <p class="list-group-item">Lista jest pusta.</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection