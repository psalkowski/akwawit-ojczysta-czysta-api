@extends('layout')

@section('body')

    <ul class="toaster">
        @if(Session::has('status'))
            <li class="alert alert-{{ Session::get('status') }} alert-dismissible" role="alert">
                {{ Session::get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </li>
        @endif
    </ul>

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default" id="panel-form">
                <div class="panel-heading">
                    <h3 class="panel-title">Formularz</h3>

                    <div class="panel-action">
                        <a href="{{ route('menu.items.create', $menu->id) }}" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span></a>
                    </div>
                </div>
                <div class="panel-body">
                    @yield('form')

                    <div class="form-disabled"></div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default panel-sortable" data-url="{{ route('menu.items.position') }}">
                <div class="panel-heading">
                    <h3 class="panel-title">Lista elementów</h3>

                    <div class="panel-action">
                        <a href="{{ route('menu.edit', $menu->id) }}" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span></a>
                        <button class="btn btn-default btn-cancel"><span class="glyphicon glyphicon-remove"></span></button>
                        <button class="btn btn-default btn-ok" data-url="{{ route('slider.position') }}"><span class="glyphicon glyphicon-ok"></span></button>
                        <button class="btn btn-default btn-sort"><span class="glyphicon glyphicon-sort"></span></button>
                    </div>
                </div>

                <div class="list-group">
                    @forelse($menu->items as $idx => $entity)
                        <a href="{{ route('menu.items.edit', [$menu->id, $entity->id]) }}" data-id="{{ $entity->id }}" class="list-group-item {{ Request::url() == (route('menu.items.edit', [$menu->id, $entity->id])) ? 'active' : null }}">
                            <span class="list-group-element">
                                <span class="number">{{$idx + 1}}.</span> {{ $entity->name }}

                                <span class="item-action">
                                    <span class="btn btn-sm btn-default btn-remove"><span class="glyphicon glyphicon-trash"></span></span>
                                </span>
                            </span>

                            <span class="list-group-remove">
                                Czy na pewno chcesz usunąć element?

                                <span class="item-action">
                                    <span class="btn btn-sm btn-default btn-remove-no"><span class="glyphicon glyphicon-remove"></span></span>
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['menu.items.destroy', $menu->id, $entity->id]]) !!}
                                    <button class="btn btn-sm btn-danger btn-remove-ok"><span class="glyphicon glyphicon-ok"></span></button>
                                    {!! Form::close() !!}
                                </span>
                            </span>
                        </a>
                    @empty
                        <p class="list-group-item">Lista jest pusta.</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection