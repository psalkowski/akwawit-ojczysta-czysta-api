@if (count($errors) > 0)
    <div class="alert alert-danger">
        <p><strong>Wystąpiły błedy:</strong></p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@include('menu.items.form-' . $template)