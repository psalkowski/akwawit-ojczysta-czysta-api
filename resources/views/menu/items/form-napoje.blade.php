<div class="form-group">
    {!! Form::label('name', 'Etykieta') !!}
    <p class="form-helper">Etykieta będzie widoczna tylko w CMS'ie w celu identyfikowania elementu. Powinna być unikalna.</p>
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('content[name]', 'Nazwa napoju') !!}
    {!! Form::text('content[name]', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('content[desc]', 'Opis') !!}
    <p class="form-helper">Opis powinien określać smaki napoju np. pomarańczowy, porzeczkowy</p>
    {!! Form::text('content[desc]', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('content[price]', 'Cena') !!}
    <p class="form-helper">Wpisz cenę bez waluty. Cena będzie pokazana w PLN.</p>
    <div class="input-group">
        {!! Form::text('content[price]', null, ['class' => 'form-control']) !!}
        <div class="input-group-addon">zł</div>
    </div>
</div>

<div class="form-group">
    <a class="btn btn-default" href="{{ route('menu.edit', $menu->id) }}">Anuluj</a>
    {!! Form::submit('Zapisz', ['class' => 'btn btn-primary']) !!}
</div>


@section('script')
    <script>
        $(document).ready(function() {
            var container = $('#kindOf');
            container.on('input', 'input[name$="[price]"]', function(e) {
                this.value = this.value.replace(/([^0-9,])/g, '');
            });
        });
    </script>
@endsection