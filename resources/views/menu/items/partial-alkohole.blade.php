<div class="panel panel-default">
    <div class="panel-heading remove-panel" role="tab">
        <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#kindOf" href="#" aria-expanded="false">
                <span class="list-group-title">Element 1</span>

                <span class="remove-action">
                    <span class="btn btn-sm btn-default btn-remove"><span class="glyphicon glyphicon-trash"></span></span>
                </span>

                <span class="remove-body">
                    Czy na pewno chcesz usunąć element?

                    <span class="remove-action">
                        <span class="btn btn-sm btn-default btn-remove-no"><span class="glyphicon glyphicon-remove"></span></span>
                        <span class="btn btn-sm btn-danger btn-remove-ok"><span class="glyphicon glyphicon-ok"></span></span>
                    </span>
                </span>
            </a>
        </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel">
        <div class="panel-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        {!! Form::label('content[__idx__][desc]', 'Opis trunku') !!}
                        <p class="form-helper">Opis trunku np. Butelka, Kieliszek.</p>
                        {!! Form::text('content[__idx__][desc]', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('content[__idx__][price]', 'Cena') !!}
                        <p class="form-helper">Cena powinna być podana bez waluty.</p>
                        <div class="input-group">
                            {!! Form::text('content[__idx__][price]', null, ['class' => 'form-control']) !!}
                            <div class="input-group-addon">zł</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('content[__idx__][name]', 'Nazwa') !!}
                <p class="form-helper">Nazwa trunku np. Królewskie 0,5L.</p>
                {!! Form::text('content[__idx__][name]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>