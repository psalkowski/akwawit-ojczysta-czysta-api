<div class="form-group">
    {!! Form::label('name', 'Etykieta') !!}
    <p class="form-helper">Etykieta będzie widoczna tylko w CMS'ie w celu identyfikowania elementu. Powinna być unikalna.</p>
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="file-container">
    <div class="form-group">
        {!! Form::label('file', 'Zdjęcie') !!}
        <p class="form-helper">Sugerowane wymiary zdjęcia to nie więcej niż 300x300px.</p>

        <div class="btn btn-success btn-file">
            Wybierz plik {!! Form::file('file', ['accept' => 'image/*']) !!}
        </div>

        <span class="badge badge-file"></span>
    </div>

    <div class="form-group">
        @if(isset($entity) and $entity->image)
            <img class="file-preview img-thumbnail" src="{{ url($entity->image) }}" alt="">
        @else
            <img class="file-preview img-thumbnail" src="{{ url('/images/blank.png') }}" alt="">
        @endif
    </div>
</div>

<div class="form-group">
    <h3>Rodzaje podawania trunku</h3>

    <div class="panel-group" id="kindOf" role="tablist" aria-multiselectable="true"
         data-session="{{ Session::has('_old_input') ? json_encode(Session::get('_old_input')['content']) : null }}"
         data-items="{{ isset($entity) ? json_encode($entity->content) : null}}"
         data-html="{{ view('menu.items.partial-alkohole') }}">
    </div>

    <hr>

    <div class="form-group" style="text-align: right;">
        <div class="btn btn-default new-item">Dodaj kolejny</div>
    </div>
</div>

<hr>

<div class="form-group">
    <a class="btn btn-default" href="{{ route('menu.edit', $menu->id) }}">Anuluj</a>
    {!! Form::submit('Zapisz', ['class' => 'btn btn-primary']) !!}
</div>


@section('script')
    <script>
        $(document).ready(function() {
            var container = $('#kindOf');
            var session = container.data('session');
            var items = container.data('items');
            var i = 0;

            if(session && session.length) {
                for(i = 0; i < session.length; i++) {
                    var item = session[i];
                    var html = $(container.data('html'));

                    html.find('.panel-heading a').attr('href', '#item-' + i).find('.list-group-title').html('Element #' + (i+1));
                    html.find('.panel-collapse').attr('id', 'item-' + i);

                    html.find('input[name="content[__idx__][desc]"]').val(item.desc);
                    html.find('input[name="content[__idx__][price]"]').val(item.price);
                    html.find('input[name="content[__idx__][name]"]').val(item.name);
                    html.find('.btn-remove-ok').data('callback', removeConfirm);

                    container.append(html);
                }
            } else if(items && items.length) {
                for(i = 0; i < items.length; i++) {
                    var item = items[i];
                    var html = $(container.data('html'));

                    html.find('.panel-heading a').attr('href', '#item-' + i).find('.list-group-title').html('Element #' + (i+1));
                    html.find('.panel-collapse').attr('id', 'item-' + i);

                    html.find('input[name="content[__idx__][desc]"]').val(item.desc);
                    html.find('input[name="content[__idx__][price]"]').val(item.price);
                    html.find('input[name="content[__idx__][name]"]').val(item.name);
                    html.find('.btn-remove-ok').data('callback', removeConfirm);

                    container.append(html);
                }
            } else {
                var html = $(container.data('html'));
                html.find('.panel-heading a').attr('href', '#item-' + i);
                html.find('.panel-collapse').attr('id', 'item-' + i);
                html.find('.btn-remove-ok').data('callback', removeConfirm);
                container.append(html);

                i++;
            }

            container.find('.panel-collapse').first().addClass('in');
            container.on('input', 'input[name$="[price]"]', function(e) {
                this.value = this.value.replace(/([^0-9,])/g, '');
            });

            $('.new-item').on('click', function() {
                var html = $(container.data('html'));
                html.find('.panel-heading a').attr('href', '#item-' + i).find('.list-group-title').html('Element #' + (i+1));
                html.find('.panel-collapse').attr('id', 'item-' + i).addClass('in');
                container.find('.panel-collapse.in').removeClass('in');
                container.append(html);

                i++;
            });

            container.on('click', '.remove-item', function() {
                $(this).closest('.panel').remove();
            });

            $('form').on('submit', function() {
                $('#kindOf .panel').each(function(i) {
                    $(this).find('input[name*="__idx__"]').each(function() {
                        $(this).attr('name', $(this).attr('name').replace('__idx__', i));
                    });
                });
            });

            function removeConfirm(element) {
                element.closest('.panel').remove();
            }
        });
    </script>
@endsection