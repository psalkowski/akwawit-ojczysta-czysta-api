<div class="form-group">
    {!! Form::label('name', 'Etykieta') !!}
    <p class="form-helper">Etykieta będzie widoczna tylko w CMS'ie w celu identyfikowania elementu. Powinna być unikalna.</p>
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('content', 'Nazwa pozycji menu') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control tinymce']) !!}
</div>

<div class="file-container">
    <div class="form-group">
        {!! Form::label('file', 'Zdjęcie') !!}
        <p class="form-helper">Sugerowane wymiary zdjęcia to 600x600px. Prosimy zachować proporcje.</p>

        <div class="btn btn-success btn-file">
            Wybierz plik {!! Form::file('file', ['accept' => 'image/*']) !!}
        </div>

        <span class="badge badge-file"></span>
    </div>

    <div class="form-group">
        @if(isset($entity) and $entity->image)
            <img class="file-preview img-thumbnail" src="{{ url($entity->image) }}" alt="">
        @else
            <img class="file-preview img-thumbnail" src="{{ url('/images/blank.png') }}" alt="">
        @endif
    </div>
</div>

<div class="form-group">
    <a class="btn btn-default" href="{{ route('menu.edit', $menu->id) }}">Anuluj</a>
    {!! Form::submit('Zapisz', ['class' => 'btn btn-primary']) !!}
</div>

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection