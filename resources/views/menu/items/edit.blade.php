@extends('menu.items.layout')

@section('form')
    {!! Form::model($entity, ['route' => ['menu.items.update', $menu->id, $entity->id], 'method' => 'PUT', 'files' => true])  !!}
        @include('menu.items.form')
    {!! Form::close() !!}
@endsection