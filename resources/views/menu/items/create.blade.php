@extends('menu.items.layout')

@section('form')
    {!! Form::open(['route' => ['menu.items.store', $menu->id], 'files' => true]) !!}
        @include('menu.items.form')
    {!! Form::close() !!}
@endsection
