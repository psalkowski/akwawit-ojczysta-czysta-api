@if (count($errors) > 0)
    <div class="alert alert-danger">
        <p><strong>Wystąpiły błedy:</strong></p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group">
    {!! Form::label('name', 'Nazwa') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('price', 'Cena') !!}
    <p class="form-helper">Wpisz cenę bez waluty. Cena będzie pokazana w PLN.</p>

    <div class="input-group">
        {!! Form::text('price', null, ['class' => 'form-control']) !!}
        <div class="input-group-addon">zł</div>
    </div>
</div>

<div class="form-group">
    <a class="btn btn-default" href="{{ route('schedule.index') }}">Anuluj</a>
    {!! Form::submit('Zapisz', ['class' => 'btn btn-primary']) !!}
</div>

@section('script')
    <script>
        $(document).ready(function() {
            $('input[name="price"]').on('input', function(e) {
                this.value = this.value.replace(/[^0-9,]/g, '');
            });
        });
    </script>
@endsection