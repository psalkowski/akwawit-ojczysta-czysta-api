@extends('menu.layout')

@section('form')
    {!! Form::model($entity, ['route' => ['menu.update', $entity->id], 'method' => 'PUT'])  !!}
    @include('menu.form')
    {!! Form::close() !!}
@endsection

@section('list')
    <div class="panel panel-default panel-sortable" data-url="{{ route('menu.items.position') }}">
        <div class="panel-heading">
            <h3 class="panel-title">Elementy menu</h3>

            <div class="panel-action">
                <button class="btn btn-default btn-cancel"><span class="glyphicon glyphicon-remove"></span></button>
                <button class="btn btn-default btn-ok" data-url="{{ route('slider.position') }}"><span class="glyphicon glyphicon-ok"></span></button>
                <button class="btn btn-default btn-sort"><span class="glyphicon glyphicon-sort"></span></button>
                <a href="{{ route('menu.items.create', [$entity->id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span></a>
            </div>
        </div>

        <div class="list-group">
            @forelse($entity->items as $idx => $item)
                <a href="{{ route('menu.items.edit', [$entity->id, $item->id]) }}" data-id="{{ $item->id }}" class="list-group-item {{ Request::url() == (route('menu.items.edit', $item->id)) ? 'active' : null }}">
                    <span class="list-group-element">
                        <span class="number">{{$idx + 1}}.</span> {{ $item->name }}

                        <span class="item-action">
                            <span class="btn btn-sm btn-default btn-remove"><span class="glyphicon glyphicon-trash"></span></span>
                        </span>
                    </span>

                    <span class="list-group-remove">
                        Czy na pewno chcesz usunąć element?

                        <span class="item-action">
                            <span class="btn btn-sm btn-default btn-remove-no"><span class="glyphicon glyphicon-remove"></span></span>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['menu.items.destroy', $entity->id, $item->id]]) !!}
                                <button class="btn btn-sm btn-danger btn-remove-ok"><span class="glyphicon glyphicon-ok"></span></button>
                            {!! Form::close() !!}
                        </span>
                    </span>
                </a>
            @empty
                <p class="list-group-item">Lista jest pusta.</p>
            @endforelse
        </div>
    </div>
@endsection