<body>

<h3>Rezerwacja</h3>
<hr>

<p><strong>Zamówienie złożył:</strong> {{ $data['name'] }}</p>
<p><strong>Telefon:</strong> {{ $data['phone'] }}</p>
<p><strong>E-mail:</strong> <a href="mailto:{{ $data['email'] }}">{{ $data['email'] }}</a></p>
<p><strong>Ilość miejsc:</strong> {{ $data['quantity'] }}</p>
<p><strong>Data rezerwacji:</strong> {{ $data['reservationDate'] }}</p>
<br>

<p><a href="http://ojczystaczysta.pl">OjczystaCzysta.pl</a></p>
</body>