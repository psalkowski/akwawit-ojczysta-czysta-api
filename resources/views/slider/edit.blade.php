@extends('slider.layout')

@section('form')
    {!! Form::model($entity, ['route' => ['slider.update', $entity->id], 'method' => 'PUT', 'files' => true])  !!}
        @include('slider.form')
    {!! Form::close() !!}
@endsection