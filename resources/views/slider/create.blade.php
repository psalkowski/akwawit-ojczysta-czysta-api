@extends('slider.layout')

@section('form')
    {!! Form::open(['route' => 'slider.store', 'files' => true])  !!}
        @include('slider.form')
    {!! Form::close() !!}
@endsection
