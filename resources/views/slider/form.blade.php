@if (count($errors) > 0)
    <div class="alert alert-danger">
        <p><strong>Wystąpiły błedy:</strong></p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<div class="form-group">
    {!! Form::label('name', 'Nazwa') !!}
    <p class="form-helper">Nazwa powinna być unikalna. Widoczna jest tylko w panelu administracyjnym w celu rozróżnienia sliderów.</p>
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Sekcja') !!}
    <p class="form-helper">Wybierz sekcje do której ma linkować zdjęcie</p>
    {!! Form::select('link', $sections, isset($entity) ? $entity->link : 'Wybierz opcje', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('alt', 'Opis') !!}
    <p class="form-helper">Podaj krótki opis zdjęcia widoczny dla wyszukiwarki</p>
    {!! Form::text('alt', null, ['class' => 'form-control']) !!}
</div>

<div class="file-container">
    <div class="form-group">
        {!! Form::label('file', 'Zdjęcie na duży ekran') !!}
        <p class="form-helper">Sugerowane wymiary zdjęcia to 1456x764px. Prosimy zachować proporcje.</p>

        <div class="btn btn-success btn-file">
            Wybierz plik {!! Form::file('file', ['accept' => 'image/*']) !!}
        </div>

        <span class="badge badge-file"></span>
    </div>

    <div class="form-group">
        @if(isset($entity) and $entity->image)
            <img class="file-preview img-thumbnail" src="{{ url($entity->image)  }}" alt="">
        @else
            <img class="file-preview img-thumbnail" src="{{ url('/images/blank.png') }}" alt="">
        @endif
    </div>
</div>

<div class="file-container">
    <div class="form-group">
        {!! Form::label('file_tablet', 'Zdjęcie na tablet') !!}
        <p class="form-helper">Sugerowane wymiary zdęcia to 991x520px. Prosimy zachować proporcje.</p>

        <div class="btn btn-success btn-file">
            Wybierz plik {!! Form::file('file_tablet', ['accept' => 'image/*']) !!}
        </div>

        <span class="badge badge-file"></span>
    </div>

    <div class="form-group">
        @if(isset($entity) and $entity->image_tablet)
            <img class="file-preview img-thumbnail" src="{{ url($entity->image_tablet)  }}" alt="">
        @else
            <img class="file-preview img-thumbnail" src="{{ url('/images/blank.png') }}" alt="">
        @endif
    </div>
</div>

<div class="file-container">
    <div class="form-group">
        {!! Form::label('file_mobile', 'Zdjęcie na telefon') !!}
        <p class="form-helper">Sugerowane wymiary zdjęcia to 767x402px. Prosimy zachować proporcje.</p>

        <div class="btn btn-success btn-file">
            Wybierz plik {!! Form::file('file_mobile', ['accept' => 'image/*']) !!}
        </div>

        <span class="badge badge-file"></span>
    </div>

    <div class="form-group">
        @if(isset($entity) and $entity->image_mobile)
            <img class="file-preview img-thumbnail" src="{{ url($entity->image_mobile)  }}" alt="">
        @else
            <img class="file-preview img-thumbnail" src="{{ url('/images/blank.png') }}" alt="">
        @endif
    </div>
</div>

<div class="form-group">
    <a class="btn btn-default" href="{{ route('slider.index') }}">Anuluj</a>
    {!! Form::submit('Zapisz', ['class' => 'btn btn-primary']) !!}
</div>