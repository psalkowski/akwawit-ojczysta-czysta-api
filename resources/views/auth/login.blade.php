<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Logowanie :: Ojczysta Czysta</title>

    <script src="https://use.typekit.net/yhw6ows.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <style>
        .panel {
            margin-top: 100px;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-sm-offset-4 col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Logowanie do CMS</div>
                </div>
                <div class="panel-body">
                    <form method="POST" class="form" action="/auth/login">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="password">Hasło</label>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="remember">
                                <input type="checkbox" name="remember" id="remember"> Zapamiętaj
                            </label>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
</body>
</html>