@extends('layout')

@section('body')

    <ul class="toaster">
        @if(Session::has('status'))
            <li class="alert alert-{{ Session::get('status') }} alert-dismissible" role="alert">
                {{ Session::get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </li>
        @endif
    </ul>

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default" id="panel-form">
                <div class="panel-heading">
                    <h3 class="panel-title">Formularz</h3>

                    <div class="panel-action">
                        <a href="{{ route('schedule.create') }}" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span></a>
                    </div>
                </div>
                <div class="panel-body">
                    @yield('form')

                    <div class="form-disabled"></div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default" id="panel-sortable">
                <div class="panel-heading">
                    <h3 class="panel-title">Lista elementów</h3>
                </div>

                <div class="list-group">
                    @forelse($entities as $entity)
                        <a href="{{ route('schedule.edit', $entity->id) }}" data-id="{{ $entity->id }}" class="list-group-item {{ Request::url() == (route('schedule.edit', $entity->id)) ? 'active' : null }}">
                            <span class="list-group-element">
                                {{ $entity->name }}

                                <span class="item-action">
                                    <span class="btn btn-sm btn-default btn-remove"><span class="glyphicon glyphicon-trash"></span></span>
                                </span>
                            </span>

                            <span class="list-group-remove">
                                Czy na pewno chcesz usunąć element?

                                <span class="item-action">
                                    <span class="btn btn-sm btn-default btn-remove-no"><span class="glyphicon glyphicon-remove"></span></span>
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['schedule.destroy', $entity->id]]) !!}
                                    <button class="btn btn-sm btn-danger btn-remove-ok"><span class="glyphicon glyphicon-ok"></span></button>
                                    {!! Form::close() !!}
                                </span>
                            </span>
                        </a>
                    @empty
                        <p class="list-group-item">Lista jest pusta.</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection