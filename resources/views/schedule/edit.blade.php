@extends('schedule.layout')

@section('form')
    {!! Form::model($entity, ['route' => ['schedule.update', $entity->id], 'method' => 'PUT'])  !!}
        @include('schedule.form')
    {!! Form::close() !!}
@endsection