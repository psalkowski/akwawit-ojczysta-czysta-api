@extends('schedule.layout')

@section('form')
    {!! Form::open(['route' => 'schedule.store'])  !!}
        @include('schedule.form')
    {!! Form::close() !!}
@endsection
