@if (count($errors) > 0)
    <div class="alert alert-danger">
        <p><strong>Wystąpiły błedy:</strong></p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="form-group">
    {!! Form::label('name', 'Nazwa') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('desc', 'Opis') !!}
    <p class="form-helper">Opis wydarzenia powinien być krótki.</p>
    {!! Form::text('desc', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('type', 'Typ') !!}
    <p class="form-helper">Typ wydarzenia odzwierciedlony będzie ikoną.</p>
    {!! Form::select('type', $types, isset($entity) ? $entity->type : 'Wybierz typ', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('date', 'Data') !!}
    <p class="form-helper">Data powinna być w formacie: DD/MM/YYYY.</p>
    {!! Form::text('date', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('time', 'Godzina') !!}
    <p class="form-helper">Godzina powinna być w formacie: HH:MM.</p>
    {!! Form::text('time', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <a class="btn btn-default" href="{{ route('schedule.index') }}">Anuluj</a>
    {!! Form::submit('Zapisz', ['class' => 'btn btn-primary']) !!}
</div>