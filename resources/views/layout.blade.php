<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{csrf_token()}}">

    <title>Dashboard :: Ojczysta Czysta</title>

    <script src="https://use.typekit.net/yhw6ows.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

    <link rel="stylesheet" href="{{ asset('/css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    @yield('style')
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ starts_with(Route::currentRouteName(), 'home') ? 'active' : null }}"><a href="{{ route('home') }}">Dashboard <span class="sr-only">(current)</span></a></li>
                <li class="{{ starts_with(Route::currentRouteName(), 'slider') ? 'active' : null }}"><a href="{{ route('slider.index') }}">Slider</a></li>
                <li class="{{ starts_with(Route::currentRouteName(), 'schedule') ? 'active' : null }}"><a href="{{ route('schedule.index') }}">Harmonogram</a></li>
                <li class="{{ starts_with(Route::currentRouteName(), 'menu') ? 'active' : null }}"><a href="{{ route('menu.index') }}">Menu</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/auth/logout') }}">Wyloguj</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    @yield('body')
</div>

@yield('outside')

<script src="{{ asset('/js/vendor.js') }}"></script>
<script src="{{ asset('/js/all.js') }}"></script>

@yield('script')

</body>
</html>