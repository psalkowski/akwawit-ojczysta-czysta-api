$(document).ready(function() {
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });

    var panelForm = $('#panel-form');

    panelForm.on('click', '.file-container .file-preview', function() {
        $(this).closest('.file-container').find('input[type="file"]').trigger('click');
    });

    panelForm.on('change', '.file-container input[type="file"]', function() {
        var value = $(this).val();
        var $this = $(this);

        value = value.replace(/\\/g, '/');
        value = value.split('/');
        value = value.pop();

        $(this).parent().next().html(value);

        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $this.closest('.file-container').find('.file-preview', panelForm).attr('src', e.target.result);
            };

            reader.readAsDataURL(this.files[0]);
        }
    });

    var removeBtn = $('.btn-remove');
    var removeNoBtn = $('.btn-remove-no');
    var removeOkBtn = $('.btn-remove-ok');

    removeBtn.click(function(e) {
        e.preventDefault();

        $(this).closest('.list-group-item').addClass('remove-confirmation');
    });

    removeNoBtn.click(function(e) {
        e.preventDefault();

        $(this).closest('.list-group-item').removeClass('remove-confirmation');
    });


    $(document).on('click', '.remove-action .btn-remove', function(e) {
        e.preventDefault();
        e.stopPropagation();

        $(this).closest('.remove-panel').addClass('remove-confirmation');
    });

    $(document).on('click', '.remove-action .btn-remove-no, .remove-action .btn-remove-ok', function(e) {
        e.preventDefault();
        e.stopPropagation();

        $(this).closest('.remove-panel').removeClass('remove-confirmation');

        var callback = $(this).data('callback');
        if(callback && $.isFunction(callback)) {
            callback($(this));
        }
    });

    tinymce.init({
        selector: '.tinymce',
        menubar: false,
        plugins: "textcolor,code",
        toolbar: "undo redo | bold italic forecolor | code",
        forced_root_block: false,
        force_p_newlines : false,
        force_br_newlines : true,
        convert_newlines_to_brs : false,
        remove_linebreaks : true,
        height: 100,
        resize: false,
        textcolor_map : [
            "ED2327", "Red"
        ]
    });
});


