$(document).ready(function() {
    var sortablePanel = $('.panel-sortable');
    var sortableBtn = $('.btn-sort', sortablePanel);
    var okBtn = $('.btn-ok', sortablePanel);
    var cancelBtn = $('.btn-cancel', sortablePanel);

    sortableBtn.click(function() {
        var sortable = $(this).closest('.panel-sortable').find('.list-group');
        var panelAction = $(this).closest('.panel-action');

        panelAction.addClass('active');

        sortable.data('html', sortable.html());
        sortable.sortable({
            axis: 'y',
            update: function(event, ui) {
                console.log(ui.item.closest('.list-group').find('.number'));
                ui.item.closest('.list-group').find('.number').each(function(idx) {
                    $(this).html((idx+1) + '.');
                });
            }
        });
    });

    okBtn.click(function() {
        var sortable = $(this).closest('.panel-sortable').find('.list-group');
        var panelAction = $(this).closest('.panel-action');
        var panel = $(this).closest('.panel-sortable');

        panelAction.removeClass('active');
        sortable.sortable('destroy');

        var url = panel.data('url');
        var items = [];

        sortable.find('a').each(function(index, element) {
            var item = {};
            item.position = index;
            item.id = $(element).data('id');
            items.push(item);
        });

        $.post(url, {items: items}, function(res) {
            $('.toaster').append(
                '<li class="alert alert-' + res.status + ' alert-dismissible" role="alert">' +
                res.message +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '</li>');
        }).fail(function(xhr,status,message) {
            if(status === 'error') status = 'danger';

            $('.toaster').append(
                '<li class="alert alert-' + status + ' alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '<strong>Wystąpił nieoczekiwany błąd:</strong><br> ' + message +
                '</li>');
        });
    });

    cancelBtn.click(function() {
        var sortable = $(this).closest('.panel-sortable').find('.list-group');
        var panelAction = $(this).closest('.panel-action');

        panelAction.removeClass('active');
        sortable.sortable('destroy');
        sortable.html(sortable.data('html'));
    });
});