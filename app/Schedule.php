<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \DateTime event_at
 */
class Schedule extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'desc', 'type', 'event_at'];

    protected $dates = ['event_at'];

    public function scopeUpcoming($query) {
        return $query->where('event_at', '>=', Carbon::now()->toDateTimeString())->orderBy('event_at', 'asc')->get();
    }

    public function scopeOrdered($query) {
        return $query->orderBy('event_at', 'asc')->get();
    }

    public function getDateAttribute() {
        if($this->event_at) {
            return $this->event_at->format('d/m/Y');
        }
        return (new \DateTime())->format('d/m/Y');
    }

    public function getTimeAttribute() {
        if($this->event_at) {
            return $this->event_at->format('G:i');
        }
        return (new \DateTime())->format('G:i');
    }

}
