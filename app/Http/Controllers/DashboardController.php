<?php

namespace App\Http\Controllers;

class DashboardController extends Controller
{
    public function index() {
        return view('welcome');
    }

    public function api() {
        return response('hello api');
    }

    public function cms() {
        return response('hello cms');
    }
}
