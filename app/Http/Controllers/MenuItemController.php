<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMenuItemRequest;
use App\Menu;
use App\MenuItem;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use League\Flysystem\NotSupportedException;

class MenuItemController extends Controller
{
    const IMAGE_PATH = 'upload/menu/';

    const TEMPLATES = [
        'jedzenie'  => ['zestawy.html', 'obiady.html', 'sniadania.html'],
        'alkohole'  => 'alkohole.html',
        'napoje'    => 'napoje.html',
        'zakaski'   => 'zakaski.html'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param $menuId
     * @return \Illuminate\Http\Response
     */
    public function index($menuId)
    {
        $entities = MenuItem::ordered();
        $template = Menu::where('id', $menuId)->value('template');
        $template = $this->getTemplate($template);
        return view('menu.items.create', compact('entities', 'menuId', 'template'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $menuId
     * @return \Illuminate\Http\Response
     */
    public function create($menuId)
    {
        $menu = Menu::ordered($menuId);
        $entities = MenuItem::ordered();
        $template = $menu->template;
        $template = $this->getTemplate($template);

        return view('menu.items.create', compact('entities', 'menu', 'template'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMenuItemRequest|Request $request
     * @param $menuId
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMenuItemRequest $request, $menuId)
    {
        $position = MenuItem::where('menu_id', $menuId)->count();


        $extra = [
            'menu_id' => $menuId,
            'position' => $position
        ];

        if($request->hasFile('file')) {

            $filename = sha1(uniqid(mt_rand(), true));
            $file = $request->file('file');
            $name = $filename . '_menu.' . $file->guessExtension();
            $image = self::IMAGE_PATH . $name;
            $file->move(public_path(self::IMAGE_PATH), $name);
            $extra['image'] = $image;
        }

        MenuItem::create($extra + $request->all());

        return back()->with(['status' => 'success', 'message' => 'Element menu został utworzony']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $menuId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($menuId, $id)
    {
        $menu =  Menu::ordered($menuId);
        $entities = MenuItem::ordered();
        $entity = MenuItem::find($id);
        $template = $menu->template;
        $template = $this->getTemplate($template);

        return view('menu.items.edit', compact('entities', 'entity', 'menu', 'template'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreMenuItemRequest $request
     * @param $menuId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMenuItemRequest $request, $menuId, $id)
    {
        $entity = MenuItem::find($id);

        if($request->hasFile('file')) {
            $filename = sha1(uniqid(mt_rand(), true));
            $file = $request->file('file');
            $name = $filename . '_menu.' . $file->guessExtension();
            $image = self::IMAGE_PATH . $name;
            $file->move(public_path(self::IMAGE_PATH), $name);

            $entity->image = $image;
        }

        $entity->update($request->all());

        return back()->with(['status' => 'success', 'message' => 'Harmonogram został zaktualizowany']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $menuId
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($menuId, $id)
    {
        MenuItem::destroy($id);

        return redirect()->route('menu.edit', $menuId)->with(['status' => 'success', 'message' => 'Harmonogram został usunięty']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function position(Request $request) {
        $items = $request->get('items');

        foreach($items as $item) {
            MenuItem::find($item['id'])->update(['position' => $item['position']]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Pozycje zostały zapisane'
        ]);
    }

    private function getTemplate($template) {
        foreach(self::TEMPLATES as $key => $value) {
            if(is_array($value)) {
                if(in_array($template, $value)) {
                    return $key;
                }
            } else {
                if($value === $template) {
                    return $key;
                }
            }
        }

        return new NotSupportedException("Not supported template: " . $template);
    }
}
