<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Requests;

class SliderController extends Controller
{
    const IMAGE_PATH = 'upload/slider/';

    const SECTIONS = [
        '' => 'Wybierz opcje',
        '#wodki' => 'Wódki',
        '#wodki-lista' => 'Lista z wódkami',
        '#harmonogram' => 'Harmonogram',
        '#rezerwacja' => 'Rezerwacja',
        '#miasta' => 'Miasta',
        '#menu' => 'Menu',
        '#poznaj' => 'Poznaj',
        '#footer' => 'Stopka'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slider::ordered();
        $sections = self::SECTIONS;

        return view('slider.create', compact('slides', 'sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $slides = Slider::ordered();
        $sections = self::SECTIONS;

        return view('slider.create', compact('slides', 'sections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\StoreSliderRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreSliderRequest $request)
    {
        $position = Slider::all()->count();
        $data = [
            'position' => $position
        ] ;

        $filename = sha1(uniqid(mt_rand(), true));
        $file = $request->file('file');
        $name = $filename . '_large.' . $file->guessExtension();
        $file->move(public_path(self::IMAGE_PATH), $name);
        $data['image'] = self::IMAGE_PATH .  $name;

        $file = $request->file('file_tablet');
        $name = $filename . '_tablet.' . $file->guessExtension();
        $file->move(public_path(self::IMAGE_PATH), $name);
        $data['image_tablet'] = self::IMAGE_PATH .  $name;

        $file = $request->file('file_mobile');
        $name = $filename . '_mobile.' . $file->guessExtension();
        $file->move(public_path(self::IMAGE_PATH), $name);
        $data['image_mobile'] = self::IMAGE_PATH .  $name;

        Slider::create($data + $request->all());

        return back()->with(['status' => 'success', 'message' => 'Slider został utworzony']);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        return response()->json(Slider::ordered());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = Slider::find($id);
        $sections = self::SECTIONS;
        $slides = Slider::ordered();

        return view('slider.edit', compact('slides', 'entity', 'sections'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\UpdateSliderRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateSliderRequest $request, $id)
    {
        $slider = Slider::find($id);
        $filename = sha1(uniqid(mt_rand(), true));
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $name = $filename . '_large.' . $file->guessExtension();
            $file->move(public_path(self::IMAGE_PATH), $name);
            $slider->image = self::IMAGE_PATH .  $name;
        }
        if($request->hasFile('file_tablet')) {
            $file = $request->file('file_tablet');
            $name = $filename . '_tablet.' . $file->guessExtension();
            $file->move(public_path(self::IMAGE_PATH), $name);

            $slider->image_tablet = self::IMAGE_PATH .  $name;
        }
        if($request->hasFile('file_mobile')) {
            $file = $request->file('file_mobile');
            $name = $filename . '_mobile.' . $file->guessExtension();
            $file->move(public_path(self::IMAGE_PATH), $name);
            $slider->image_mobile = self::IMAGE_PATH .  $name;
        }

        $slider->update($request->all());
        return back()->with(['status' => 'success', 'message' => 'Slider został zaktualizowany']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slider::destroy($id);

        return redirect()->route('slider.index')->with(['status' => 'success', 'message' => 'Slider został usunięty']);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function position(Request $request) {
        $items = $request->get('items');

        foreach($items as $item) {
            Slider::find($item['id'])->update(['position' => $item['position']]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Pozycje zostały zapisane'
        ]);
    }
}
