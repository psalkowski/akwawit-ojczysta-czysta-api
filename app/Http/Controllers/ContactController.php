<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;

class ContactController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request)
    {
        $emails = [
            'Kraków' => 'krakow@ojczystaczysta.pl',
            'Warszawa' => 'warszawa@ojczystaczysta.pl',
            'Wrocław' => 'wroclaw@ojczystaczysta.pl'
        ];
        $data = $request->all();

        Mail::send('contact.email', ['data' => $data], function ($m) use ($data, $emails) {
            $email = $emails[$data['city']];

            $m
                ->to($email, 'Formularz kontaktowy')
                ->subject('Rezerwacja - Ojczysta Czysta');
        });

        Mail::send('contact.email', ['data' => $data], function ($m) use ($data, $emails) {
            $email = $emails[$data['city']];

            $m
                ->to($data['email'], $data['name'])
                ->replyTo($email, 'Ojczysta Czysta')
                ->subject('Rezerwacja - Ojczysta Czysta');
        });

        return response()->json('success');
    }
}
