<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use League\Flysystem\NotSupportedException;

class MenuController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entities = Menu::ordered();
        return view('menu.index', compact('entities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        throw new NotSupportedException();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        Menu::create($data);

        return back()->with(['status' => 'success', 'message' => 'Menu zostało utworzone']);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        return response()->json(Menu::ordered());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entities = Menu::ordered();
        $entity = Menu::ordered($id);
//        $entity = Menu::where('id', $id)->with(['items' => function($query) {
//            $query->orderBy('position', 'asc');
//        }])->first();

        return view('menu.edit', compact('entities', 'entity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entity = Menu::find($id);

        $data = $request->all();
        $entity->update($data);

        return back()->with(['status' => 'success', 'message' => 'Menu zostało zaktualizowane']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Menu::destroy($id);

        return redirect()->route('menu.index')->with(['status' => 'success', 'message' => 'Menu zostało usunięte']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function position(Request $request) {
        $items = $request->get('items');

        foreach($items as $item) {
            Menu::find($item['id'])->update(['position' => $item['position']]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Pozycje zostały zapisane'
        ]);
    }
}
