<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreScheduleRequest;
use App\Schedule;
use Illuminate\Http\Request;
use App\Http\Requests;

class ScheduleController extends Controller
{
    const TYPES = [
        '' => 'Wybierz typ',
        'icon-football' => 'Piłka nożna',
        'icon-volleyball' => 'Siatkówka',
        'icon-basketball' => 'Koszykówka',
        'icon-disco' => 'Impreza taneczna'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = self::TYPES;
        $entities = Schedule::ordered();
        return view('schedule.create', compact('entities', 'types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = self::TYPES;
        $entities = Schedule::ordered();
        return view('schedule.create', compact('entities', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreScheduleRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreScheduleRequest $request)
    {
        $data = $request->all();
        $data['event_at'] = \DateTime::createFromFormat('d/m/Y G:i', $data['date'] . ' ' . $data['time']);
        Schedule::create($data);

        return back()->with(['status' => 'success', 'message' => 'Harmonogram został utworzony']);
    }

    /**
 * Display the specified resource.
 *
 * @return \Illuminate\Http\JsonResponse
 */
    public function all()
    {
        return response()->json(Schedule::upcoming());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $types = self::TYPES;
        $entities = Schedule::ordered();
        $entity = Schedule::find($id);

        return view('schedule.edit', compact('entities', 'types', 'entity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreScheduleRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreScheduleRequest $request, $id)
    {
        $entity = Schedule::find($id);

        $data = $request->all();
        $data['event_at'] = \DateTime::createFromFormat('d/m/Y G:i', $data['date'] . ' ' . $data['time']);

        $entity->update($data);

        return back()->with(['status' => 'success', 'message' => 'Harmonogram został zaktualizowany']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Schedule::destroy($id);

        return redirect()->route('schedule.index')->with(['status' => 'success', 'message' => 'Harmonogram został usunięty']);
    }
}
