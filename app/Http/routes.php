<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['domains' => ['api.ojczystaczysta.dev', 'api.ojczystaczysta.pl'], 'middleware' => 'cors', 'prefix' => 'v1'], function() {
    Route::get('/schedule', 'ScheduleController@all');
    Route::get('/menu', 'MenuController@all');
    Route::get('/slider', 'SliderController@all');
    Route::post('/contact', 'ContactController@send');
});

Route::group(['domains' => ['cms.ojczystaczysta.dev', 'cms.ojczystaczysta.pl']], function() {
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');
    Route::get('auth/logout', 'Auth\AuthController@getLogout');

    Route::group(['middleware' => 'auth'], function() {
        Route::get('/', ['as' => 'home', 'uses' => 'DashboardController@index']);

        Route::post('/menu/position', ['as' => 'menu.position', 'uses' => 'MenuController@position']);
        Route::post('/menu/items/position', ['as' => 'menu.items.position', 'uses' => 'MenuItemController@position']);
        Route::post('/slider/position', ['as' => 'slider.position', 'uses' => 'SliderController@position']);
        Route::resource('slider', 'SliderController', ['except' => 'show']);
        Route::resource('schedule', 'ScheduleController', ['except' => 'show']);
        Route::resource('menu', 'MenuController', ['except' => ['create', 'show']]);
        Route::resource('menu.items', 'MenuItemController', ['except' => 'show']);

    });
});

