<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ArrayServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('in_array', function($attribute, $value, $parameters, $validator) {
//            dd([
//                'attribute' => $attribute,
//                'value' => $value,
//                'parameters' => $parameters,
//                'validator' => $validator
//            ]);

            foreach($value as $item) {
                $validator = Validator::make($item, [
                ]);
            }

            return $value == 'foo';
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
