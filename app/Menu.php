<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'position', 'price'];

    /**
     * Get the comments for the blog post.
     */
    public function items()
    {
        return $this->hasMany('App\MenuItem');
    }

    public function scopeOrdered($query, $id = null) {
        $query = $query->orderBy('position', 'asc')->with(['items' => function($q) {
            $q->orderBy('position', 'asc');
        }]);

        if($id) {
            return $query->where('id', $id)->first();
        }

        return $query->get();
    }
}
