<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['size', 'content', 'image', 'position', 'menu_id', 'name'];

    public function scopeOrdered($query) {
        return $query->orderBy('position', 'asc')->get();
    }

    public function getContentAttribute($value) {
        $unserializedData = @unserialize($value);

        if($unserializedData !== false) {
            return $unserializedData;
        }

        return $value;
    }

    public function setContentAttribute($value) {
        if(is_array($value)) {
            $this->attributes['content'] = serialize($value);
        } else {
            $this->attributes['content'] = $value;
        }
    }
}
