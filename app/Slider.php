<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'alt', 'image', 'image_tablet', 'image_mobile', 'link', 'position'];

    public function scopeOrdered($query) {
        return $query->orderBy('position', 'asc')->get();
    }
}
